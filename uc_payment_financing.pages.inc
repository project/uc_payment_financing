<?php


function uc_payment_financing_finalize() {
  if (!$_SESSION['do_complete']) {
    drupal_goto('cart');
  }
  $order = uc_order_load(arg(3));
  // Add a comment to let sales team know this came in through the site.
  uc_order_comment_save($order->order_id, 0, t('Order created through website (with financing !years years, APR !apr%, EAR !ear%).',array("!years"=>$_SESSION['financing_duration'], "!apr" => $_SESSION['financing_apr'], "!ear" => $_SESSION['financing_ear'])), 'admin');
  $output = uc_cart_complete_sale($order, variable_get('uc_new_customer_login', FALSE));

  watchdog('financing', 'ORDER CREATED, order #!order_id', array('!order_id'=>$order_id),WATCHDOG_NOTICE);

  //$output = uc_order_status_data($order->order_status, 'state');
  return $output;
}


